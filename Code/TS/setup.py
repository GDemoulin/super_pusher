"""This is the RTIR setup module.

This module manages the setup of the RTIR module:
- variables
- configuration
- API url

version = '2.0'
author = 'Mathieu Deloitte'
date = '2016-08-30'
updated = '2016-09-14'
"""

import os

import yaml

import rt

with open("/home/infosec/Super_pusher/config", 'r') as ymlfile:
    cfg = yaml.load(ymlfile)
assemblyline_url = cfg['Connections']['AssemblyLine']['Adresse']

al_submission_detail_path = '/submission_detail.html?sid=%s'
al_submission_detail_url = os.path.join(assemblyline_url, al_submission_detail_path)

# RTIR
host = cfg['Connections']['RTIR']['Adresse']
port = cfg['Connections']['RTIR']['Port']
user = cfg['Connections']['RTIR']['User']
password = cfg['Connections']['RTIR']['Password']
queue = cfg['Connections']['RTIR']['Queue']


def rtir_connection():
    return rt.Rt("%s://%s:%s/%s" % ('https', host, port, 'REST/1.0/'),
                 user, password, default_queue=queue)


def get_al_submission_detail_url(sid):
    return al_submission_detail_url % sid
