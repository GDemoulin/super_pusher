import json
import logging

import yaml


def elk(sub):
    with open("/home/infosec/Super_pusher/config", 'r') as ymlfile:
        cfg = yaml.load(ymlfile)
    service_wanted = cfg['ELK']
    log = logging.getLogger('log')
    log.info('ELK: Logging %s', sub['files'][0][0])

    result = {}
    cur_file = sub['files'][0][0]
    cur_file_id = str(sub['files'][0][1])
    result[cur_file] = {}

    result[cur_file] = get_result(sub, cur_file_id, service_wanted)

    result[cur_file]['Childrens'] = get_childrens(sub['file_tree'][cur_file_id]['children'], service_wanted, sub)

    with open(cfg['Out_ELK'], 'a') as outfile:
        json.dump(result, outfile)
        outfile.write("\n")


def get_childrens(tree, wanted, sub):
    temp = {}
    for child_id, info in tree.items():
        temp[info['name'][0]] = get_result(sub, child_id, wanted)
        if info['children']:
            temp[info['name'][0]]['Childrens'] = get_childrens(info['children'], wanted, sub)
    return temp


def get_subsection(sections):
    result = []
    for section in sections:
        tmp = {}

        if not section['subsections']:
            tmp[section['title_text']] = section['body']
        else:
            temp = {}
            for item in get_subsection(section['subsections']):
                if len(item) == 1 and isinstance(item, dict):
                    for key in item.keys():
                        temp[key] = item[key]
            tmp[section['title_text']] = temp

        result.append(tmp)
    return result


def get_result(sub, cur_file_id, wanted):
    result = {}
    for key in sub['results']:
        name = sub['results'][key]['response']['service_name']
        if cur_file_id in key and name in wanted:
            result[name] = get_subsection(sub['results'][key]['result']['sections'])
            if len(result[name]) == 1:
                result[name] = result[name][0]
        result['File_info'] = {}
    for key in sub['file_infos'][cur_file_id]:
        if not key.startswith('_'):
            result['File_info'][key] = sub['file_infos'][cur_file_id][key]

    return result
