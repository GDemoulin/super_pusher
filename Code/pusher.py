import logging
import os
import time
from logging.handlers import RotatingFileHandler

import yaml
from assemblyline_client import Client, ClientError
from requests import ConnectionError


def _main():
    with open("/home/infosec/Super_pusher/config", 'r') as ymlfile:
        cfg = yaml.load(ymlfile)

    # Initialize the logger

    log = logging.getLogger('pusher')
    handler = RotatingFileHandler(cfg['Log'] + 'Pusher.log')
    formater = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')
    log.setLevel(logging.INFO)
    handler.setFormatter(formater)
    log.addHandler(handler)

    log.info("Config loaded: %s", str(cfg))

    # Setting working parameters
    nq = str(cfg['Connections']['AssemblyLine']['Queue'])
    users = cfg['Connections']['AssemblyLine']['Users']
    directories = []
    for key in cfg['Source'].keys():
        directories.append([cfg['Source'][key]['Directory'], key])
    timer = cfg['Sleep_Timer']

    clients = {}
    for user in users:
        try:
            clients[user[0]] = Client(cfg['Connections']['AssemblyLine']['Adresse'], apikey=(user[0], user[1]),
                                      verify=False)
        except (ClientError, ConnectionError) as e:
            log.error("While connecting to profile %s, %s", user[0], e[0])
            exit(0)

    while True:
        for directory in directories:
            for eml in os.listdir(directory[0]):
                profile = directory[1]
                try:
                    log.info("Sending: %s to profile %s on queue %s", eml, profile, nq)
                    clients[profile].ingest(directory[0] + eml, params={'ignore_cache': True}, nq=nq)
                    os.remove(directory[0] + eml)
                except ClientError as e:
                    log.error("While sending %s to profile %s, %s", eml, profile, e[0])

        time.sleep(timer)


if __name__ == '__main__':
    _main()
