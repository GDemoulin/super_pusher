import logging
import sys
import time
from logging.handlers import RotatingFileHandler
from multiprocessing import Process, Queue, Lock

import OpenSSL
import yaml
from assemblyline_client import Client, ClientError
from socketIO_client import ConnectionError

from ELK import elk
from TS import ticket


# http://code.activestate.com/recipes/410695-exception-based-switch-case/
class CaseSelector(Exception):
    def __init__(self, value):  # overridden to ensure we've got a value argument
        Exception.__init__(self, value)


def switch(variable):
    raise CaseSelector(variable)


def case(value):
    exclass, exobj, tb = sys.exc_info()
    if exclass is CaseSelector and exobj.args[0] == value:
        return exclass
    return None


# sub-process function
def f(sources, queue, lock, i, client, max_tries):
    log = logging.getLogger('log')

    while True:
        sid = queue.get()
        sub = None
        tries = 0
        while sub is None and tries < max_tries:
            try:
                sub = client.submission.full(sid)
            except OpenSSL.SSL.Error as e:
                if tries == max_tries:
                    with lock:
                        log.error("Could not retreive  submission " + sid + "\n SSL error: " + e[0])
                else:
                    tries = tries + 1
                    time.sleep(1)
                pass

        source = sub['submission']['submitter']
        with lock:
            log.info('---Worker %s---', i)
            log.info('Received result for %s on worker %s', sub['files'][0][0], i)
            log.info('Source: %s', source)
        for module in sources[source]['Module']:
            if sources[source]['Module'][module]:
                try:
                    switch(module)
                except case('TS'):
                    if sub['submission']['max_score'] >= sources[source]['Module'][module]['Threshold']:
                        with lock:
                            log.info('TS')
                        ticket(sub)
                except case('ELK'):
                    with lock:
                        log.info('ELK')
                    elk(sub)


def _main():
    with open('/home/infosec/Super_pusher/config', 'r') as ymlfile:
        cfg = yaml.load(ymlfile)

    # Initialize the logger
    log = logging.getLogger('log')
    handler = RotatingFileHandler(cfg['Log'] + 'Listen_result.log')
    formater = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')
    log.setLevel(logging.INFO)
    handler.setFormatter(formater)
    log.addHandler(handler)

    log.info("Config loaded: %s", str(cfg))

    # Setting working parameters
    nq = str(cfg['Connections']['AssemblyLine']['Queue'])
    user = cfg['Connections']['AssemblyLine']['Users'][0]
    timer = cfg['Sleep_Timer']
    queue = Queue()
    lock = Lock()
    pool = []
    max_tries = 10
    try:
        client = Client(cfg['Connections']['AssemblyLine']['Adresse'], apikey=(user[0], user[1]), verify=False)
    except (ClientError, ConnectionError) as e:
        log.error("While connecting to profile %s, %s", user[0], e[0])
        exit(0)

    log.info('Creating %s worker(s)', str(cfg['Max_Process']))
    for i in range(cfg['Max_Process']):
        p = Process(target=f, args=(cfg['Source'], queue, lock, i + 1, client, max_tries))
        p.daemon = True
        pool.append(p)

    log.info('Starting worker(s)')
    for p in pool:
        p.start()

    log.info('Starting to listen')
    while True:
        for msg in client.ingest.get_message_list(nq):
            log.info('Received message for %s', str(msg['alert']['sid']))
            queue.put(msg['alert']['sid'])
        time.sleep(timer)


if __name__ == '__main__':
    _main()
